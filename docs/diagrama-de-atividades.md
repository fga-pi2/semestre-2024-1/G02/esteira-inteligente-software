# Diagrama de Atividades

![diagrama_de_atividades_v1](./assets/images/Diagrama_de_Atividades.png)
<div style="text-align: center;">
<b>Figura 1: Diagrama de Atividades v1</b> (Fonte: Autoria Própria)
</div>

## Versionamento
| Data | Versão | Descrição | Autor(es) |
|------|------|------|------|
|30/04/2024|1.0| Criação do Diagrama de Atividades |[Matheus Henrick](https://github.com/MatheusHenrickSantos)|


