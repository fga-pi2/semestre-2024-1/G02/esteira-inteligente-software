# Diagrama de Componentes
## Introdução
O diagrama de Componentes é um tipo de diagrama UML que serve para mostrar as relações estruturais entre os componentes de um sistema. Isso ajuda na modularização do sistema e favorece a reutilização dos mesmos.

## Diagrama de Componentes V1
![alt text](./assets/images/Diagrama%20de%20Componentes.png)
<div style="text-align: center;">
<b>Figura 1: Diagrama de Componentes V1</b> (Fonte: Autoria Própria)
</div>

## Versionamento

| Data       | Versão | Descrição                  | Autor(es)                                       | Revisor |
| ---------- | ------ | -------------------------- | ----------------------------------------------- | ------- |
| 30/04/2024 | 1.0    | Diagrama de Componentes V1 | [Danilo Domingo](https://github.com/danilow200) | [Matheus Silverio](https://github.com/MattSilverio) |