# Requisitos de Software

## Introdução

Requisitos de software são as especificações funcionais e não funcionais que definem o que um sistema deve fazer e como ele deve se comportar. Eles são essenciais para guiar o processo de desenvolvimento, ajudando a garantir que o produto final atenda às necessidades e expectativas dos usuários. A engenharia de requisitos é o conjunto de atividades e processos utilizados para elicitar, analisar, documentar e gerenciar esses requisitos ao longo do ciclo de vida de um projeto.

A correta gestão dos requisitos é crucial para o sucesso de qualquer projeto. Isso envolve identificar e resolver ambiguidades, conflitos e mudanças de requisitos de forma eficiente, garantindo que o produto desenvolvido seja de alta qualidade e cumpra seu propósito de forma eficaz. Além disso, a comunicação clara e contínua entre todos os envolvidos no projeto é fundamental para assegurar que as necessidades dos usuários sejam adequadamente traduzidas em requisitos claros e alcançáveis.

## Requisitos Funcionais

Um esboço dos requisitos foram decididos em uma reunião presencial, onde a equipe de software colaborou ativamente na definição e esboço das especificações. Posteriormente, esses requisitos foram organizados e detalhados utilizando a plataforma [Miro](https://miro.com/app/board/uXjVKQT0lBc=/), facilitando a visualização, discussão e acompanhamento por todos os membros envolvidos no projeto.


**Tabela 1: Requisitos Funcionais**

|   ID   |                                            Requisito                                            |
| :----: | :---------------------------------------------------------------------------------------------: |
| RFSO01 |                     O sistema deve permitir que os usuários cadastrem kits                      |
| RFSO02 |                     O sistema deve permitir que os usuários editem os kits                      |
| RFSO03 |                     O sistema deve permitir que os usuários listem os kits                      |
| RFSO04 |                     O sistema deve permitir que os usuários excluam os kits                     |
| RFSO05 |                     O sistema deve permitir que usuários sejam cadastrados                      |
| RFSO06 |                       O sistema deve permitir que usuários sejam editados                       |
| RFSO07 |                       O sistema deve permitir que usuários sejam listados                       |
| RFSO08 |                      O sistema deve permitir que usuários sejam excluídos                       |
| RFSO09 |                 O sistema deve permitir que os usuários cadastre os componentes                 |
| RFSO10 |                  O sistema deve permitir que os usuários editem os componentes                  |
| RFSO11 |                  O sistema deve permitir que os usuários listem os componentes                  |
| RFSO12 |                  O sistema deve permitir que os usuários exclua os componentes                  |
| RFSO13 |            O sistema deve ser capaz de reconhecer as imagens dos elementos dos kits             |
| RFSO14 |  O sistema deve ser capaz de comparar os dados da balança e das imagens dos componentes do kit  |
| RFSO15 |                    O sistema deve ser capaz de realizar controle de estoque                     |
| RFSO16 | O sistema deve ser capaz de informar quantos e quais componentes, ao concluir a montagem do kit |
| RFSO17 |             O sistema deve ser capaz de avaliar a qualidade dos componentes do kit              |
| RFS018 |               O sistema deve permitir que o usuários selecione kits para produção               |
| RFS019 |     O sistema deve permitir que o usuários se autentique no sistema com o seu devido papel      |

## Requisitos Não Funcionais

**Tabela 2: Requisitos não funcionais de software**

|   ID    |                                                               Requisito                                                               |
| :-----: | :-----------------------------------------------------------------------------------------------------------------------------------: |
| RNFSO01 |                                 Os usuários do sistemas serão separados por administrador e operador                                  |
| RNFSO02 |                                A detecção e reconhecimento de componentes devem ocorrer em tempo real                                 |
| RNFSO03 |                         O sistema deve ser capaz de lidar com múltiplas solicitações de CRUD simultaneamente                          |
| RNFSO04 | O sistema deve ser capaz de suportar a adição de novos tipos de componentes e atualizações  sem interromper as operações em andamento |
| RNFSO05 |                          O sistema deve ser robusto o suficiente para lidar co possíveis falhas de hardware                           |
| RNFSO06 |                                       A interface do usuário deve ser intuitiva e fácil de usar                                       |
| RNFSO07 |                                           Mensagens de erro devem ser claras e informativas                                           |
| RNFSO08 |                  Deve ser implementado um sistema de registro de logs abrangente para rastrear atividades do sistema                  |
| RNFSO09 |                                              O backend sera desenvolvido em Python/Django                                              |
| RNFSO10 |                                            O frontend sera desenvolvido em typescript/React                                            |
| RNFSO11 |             O sistema deve ser uma aplicação web acessível através de navegadores web padrão, como Chrome, Firefox e etc.             |
| RNFSO12 |                                              O banco de dados sera desenvolvido em MySQL                                              |

## Histórico de Versão

|    Data    | Versão |                   Descrição                    |                     Autor(es)                      |                      Revisor                       |
| :--------: | :----: | :--------------------------------------------: | :------------------------------------------------: | :------------------------------------------------: |
| 23/04/2024 |  1.0   |              Criação do Documento              |           Gabrielle, Jefferson e Heitor            |                         -                          |
| 23/04/2024 |  1.1   |              Criação dos RF e RNF              |           Gabrielle, Jefferson e Heitor            |                         -                          |
| 24/04/2024 |  1.2   |             Adicionando introdução             |           Gabrielle, Jefferson e Heitor            | Danilo, Matheus Henrick, Matheus Silverio e Heitor |
| 27/04/2024 |  1.3   | Adicionando RFS018 e RFS019, alterando RNFSO01 | Danilo, Matheus Henrick, Matheus Silverio e Heitor |               Gabrielle e Jefferson                |
