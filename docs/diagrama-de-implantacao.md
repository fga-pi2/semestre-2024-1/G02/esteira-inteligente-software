# Diagrama de Implantação

## Introdução

A implantação de software é uma fase crucial no ciclo de vida de desenvolvimento de um sistema, pois envolve a distribuição dos componentes de software em um ambiente de execução. Um diagrama de implantação é uma representação visual dessa distribuição, que ajuda a compreender a arquitetura do sistema e a tomar decisões sobre a infraestrutura necessária.

## Diagrama de Implantação V1

![Diagrama de Implantação](./assets/images/diagrama-de-implatacao.png)
<div style="text-align: center;">
<b>Imagem 1: Diagrama de Implantação V1</b> (Fonte: Autoria Própria)
</div>

## Histórico de Versão

| Data       | Versão | Descrição           | Autor(es)                                                   | Revisor |
| ---------- | ------ | ------------------- | ----------------------------------------------------------- | ------- |
| 05/06/2024 | 1.0 | Criação do artefato | Danilo, Heitor Marques, Jefferson França e Matheus Silveiro | Matheus Enrick |