# Diagrama de Classes

![diagrama_de_classes_v1](./assets/images/Diagrama_de_Classes.png)
<div style="text-align: center;">
<b>Figura 1: Diagrama de Classes v1</b> (Fonte: Autoria Própria)
</div>

## Versionamento
| Data | Versão | Descrição | Autor(es) |
|------|------|------|------|
|30/04/2024|1.0| Criação do Diagrama de Classes |[Matheus Henrick](https://github.com/MatheusHenrickSantos)|



