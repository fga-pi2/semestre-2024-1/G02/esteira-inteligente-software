# Diagrama de Pacotes

## Introdução

O diagrama de pacotes na engenharia de software é uma ferramenta da UML que organiza os elementos de um sistema em grupos lógicos. Ele mostra a arquitetura geral do sistema, destacando relações e dependências entre os pacotes, cada um representando um conjunto de elementos relacionados. Essa visualização facilita a compreensão da estrutura modular do sistema, auxiliando no desenvolvimento, manutenção e evolução do software. Em resumo, o diagrama de pacotes é essencial para planejar e visualizar a arquitetura de sistemas de software de maneira organizada e eficiente.

## Diagrama de Pacotes
![alt text](./assets/images/diagrama_pacotes.jpeg)
<center>
<b>Figura 1: Diagrama de Pacotes</b> (Fonte: Autoria Própria)
</center>

## Histórico de Versão

| Data       | Versão | Descrição           | Autor(es)                                    | Revisor                                             |
| ---------- | ------ | ------------------- | -------------------------------------------- | --------------------------------------------------- |
| 03/05/2024 | 1.0    | Criação do artefato | Jefferson França, Heitor Marques e Gabrielle | [Matheus Silverio](https://github.com/MattSilverio) |