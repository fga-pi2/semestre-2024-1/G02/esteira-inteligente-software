# Diagrama de Caso de Uso

## Introdução

O diagrama de casos de uso na engenharia de software é uma representação das interações entre usuários e o sistema. Ele descreve funcionalidades do sistema do ponto de vista do usuário, destacando casos de uso e suas relações. Essencial para capturar e documentar requisitos funcionais, facilita a comunicação entre stakeholders e define o escopo do sistema.

## Diagrama de Caso de Uso
![alt text](./assets/images/diagrama_caso_de_uso.jpeg)
<center>
<b>Figura 1: Diagrama de Caso de Uso</b> (Fonte: Autoria Própria)
</center>

## Histórico de Versão

| Data       | Versão | Descrição           | Autor(es)                                    | Revisor |
| ---------- | ------ | ------------------- | -------------------------------------------- | ------- |
| 03/05/2024 | 1.0    | Criação do artefato | Jefferson França, Heitor Marques e Gabrielle | Danilo  |