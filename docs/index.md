<style>
.fotos {
  border: 3px solid #1C5739; 
  border-radius: 999px;
  transition: ease-in-out 200ms;
}

.fotos:hover {
  border-color: #309c64;
  box-shadow: 0 0 15px #1C5739;
}  
</style>

# Documentação de Software Esteira Inteligente

**Disciplina**:  PROJETO INTEGRADOR DE ENGENHARIA 2<br>
**Código da Disciplina**: FGA0250<br>
**Número do Grupo**: 02<br>

## Sobre

Repositório referente a documentação de Software do projeto de PI 2 - [Esteira Inteligente](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs)

## Desenvolvedores

<div style="display: flex; gap: 10px; justify-content: space-around">
  <div style="display: grid; justify-items: center">
    <a href="https://gitlab.com/danilow200" target="_blank">
      <img class="fotos" src="./assets/images/membros/danilo.jpeg" width="300px" height="300px" />
    </a>
    <h4>Danilo Domingo</h4>
  </div>

  <div style="display: grid; justify-items: center">
    <a href="https://gitlab.com/heitormsb" target="_blank">
      <img class="fotos" src="./assets/images/membros/heitor.jpeg" width="300px" height="300px"  />
    </a>
    <h4>Heitor Barbosa</h4>
  </div>
  <div style="display: grid; justify-items: center">
    <a href="https://gitlab.com/Fran6s" target="_blank">
      <img class="fotos" src="./assets/images/membros/jefferson.jpeg" width="300px" height="300px"  />
    </a>
    <h4>Jefferson França</h4>
  </div>
</div>
<div style="display: flex; gap: 10px; justify-content: space-around">
  <div style="display: grid; justify-items: center">
    <a href="https://gitlab.com/MatheusHenrickSantos" target="_blank">
      <img class="fotos" src="./assets/images/membros/henrick.jpeg" width="300px" height="300px"  />
    </a>
    <h4>Matheus Henrick</h4>
  </div>
  <div style="display: grid; justify-items: center">
    <a href="https://gitlab.com/MattSilverio" target="_blank">
      <img class="fotos" src="./assets/images/membros/silveiro.jpeg" width="300px" height="300px"  />
    </a>
    <h4>Matheus Silveiro</h4>
  </div>
</div>