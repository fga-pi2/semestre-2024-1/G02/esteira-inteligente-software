# Protótipo de Alta Fidelidade

O protótipo de alta fidelidade foi feito tendo em vista as decisões tomadas na guia de estilo e nos requisitos levantados, a ideia desse protótipo é representar com precisão o que se espera do produto final.

Para o montagem do protótipo foi usada a ferramenta de prototipagem do [Figma](https://www.figma.com).

## 1. Versão 1.0 - Protótipo

Protótipo disponível em: [https://www.figma.com/design/GhF4ugEQ0wWMHDUpNlY4xX/Prot%C3%B3tipo-Esteira?node-id=0-1&t=7nMWQjsggxCrTQ0e-1](https://www.figma.com/design/GhF4ugEQ0wWMHDUpNlY4xX/Prot%C3%B3tipo-Esteira?node-id=0-1&t=7nMWQjsggxCrTQ0e-1)

### 1.2. Telas protótipo

![alt text](./assets/images/prototipo/Login.png)
<center>
<b>Imagem 1: Tela de Login</b> (Fonte: Autoria Própria)
</center>

![alt text](./assets/images/prototipo/Kits.png)
<center>
<b>Imagem 2: Tela de Seleção de Kit</b> (Fonte: Autoria Própria)
</center>

![alt text](./assets/images/prototipo/Aviso.png)
<center>
<b>Imagem 3: Tela de Aviso</b> (Fonte: Autoria Própria)
</center>

![alt text](./assets/images/prototipo/Error.png)
<center>
<b>Imagem 4: Tela de Erro</b> (Fonte: Autoria Própria)
</center>

![alt text](./assets/images/prototipo/Sucesso.png)
<center>
<b>Imagem 5: Tela de Sucesso</b> (Fonte: Autoria Própria)
</center>

## Histórico de Versão

| Data       | Versão | Descrição           | Autor(es)                         | Revisor                                             |
| ---------- | ------ | ------------------- | --------------------------------- | --------------------------------------------------- |
| 01/05/2024 | 1.0    | Criação do protótipo | Danilo Domingo e Matheus Enrick | Jefferson França |