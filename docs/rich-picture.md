# Rich picture

## 1. Introdução

Rich Picture é uma técnica visual que ajuda a compreender complexidades em um sistema, projeto ou situação de maneira mais holística. A técnica do Rich Picture envolve a criação de uma imagem ou diagrama que representa um sistema em questão, com todos os seus elementos, relacionamentos e nuances. Ao fornecer uma representação visual do sistema, o Rich Picture pode ajudar a criar uma compreensão compartilhada de todos os elementos e suas interações, facilitando assim a tomada de decisões.

## 2. Metodologia

A metodologia utilizada para desenvolvimento dos Rich Picture aqui presentes consistem na investigação analítica junto com os stakeholders, afim de identificar todos os elementos que compõem o sistema durante o processo da montagem de um Kit na esteira inteligente.


## 3. Rich Picture

### 3.1 - Visão Geral 

![rich_picture_v1](./assets/images/rich_picture_visao_geral.png)

<h6 align = "center">Figura 1: Rich Picture v1</h6>

A primeira versão do Rich Picture, representa o entendimento inicial do sistema.


### 3.2 - Exemplo com Tipos de Usuários

- Engenheiro de Produto: Tem permissões de Criar/Editar/Listar/Excluir para as entidades de kit e item, afim de manter o cadastro dos kits produzidos e com quais peças serão utilizadas, garantindo a manutenção do seu catálogo e do estoque.

![rich_picture_eng_produto](./assets/images/rich_picture_eng_produto.png)

<h6 align = "center">Figura 2: Rich Picture - Engenheiro De produto</h6>


- Operador: Tem permissões de Listar e Mandar para produção, além de que irá interagir fora do sistema com a reposição de itens que estiverem faltando em estoque.


![rich_picture_eng_produto](./assets/images/rich_picture_operador.png)

<h6 align = "center">Figura 3: Rich Picture - Operador</h6>

## Versionamento
| Data | Versão | Descrição | Autor(es) | Revisor(es) |
|------|------|------|------|------|
|26/04/2024|1.0| Criação do Documento de Rich Picture |[Matheus Silverio](https://github.com/MattSilverio)| |
|01/05/2024|1.1| Adição dos Rich Pictures por visão de tipo de usuário |[Matheus Silverio](https://github.com/MattSilverio)| |
