# Documento de Visão
## 1. Introdução
### 1.1. Finalidade
Este documento tem como objetivo apresentar um panorama claro e em detalhes que é compartilhado pela equipe de desenvolvimento do projeto da Esteira Inteligente a respeito da proposta de projeto. Desse modo, serão apresentados diversos aspectos que permeiam a execução desse software, mas sem que haja a necessidade do conhecimento de termos técnicos pelo leitor.

### 1.2. Escopo
O projeto Esteira Inteligente busca solucionar o problema na linha de produção de kits de projetos sendo montados com peças erradas, sendo que é algo recorrente vim a peça errada ou na quantidade errada, o projeto visa automatizar essa questão para minimizar esta falhas.

### 1.3. Visão Geral
O presente documento apresenta aspectos fundamentais para a compreensão do produto. Sendo um documento de caráter informativo, ele traz um organização em tópicos aonde são tratados detalhes sobre a introdução, o posicionamento, a descrição das partes envolvidas, um visão geral do produto, os recursos do produto e suas restrições, os intervalos de qualidade e as prioridades e precedências.

## 2. Posicionamento

### 2.1. Oportunidade de Negócio

Um dos membros da disciplina estagiá na fabrica da Rossi, nela existe um grande problema em sua linha de produção, os seus kits de montagem costumam apresentar problemas na contagem de suas peças e a momentos que vem com a peça errado, para reduzir esse erro foi pensado em uma solução que utiliza de Esteira Inteligente, que irá entregar as peças de acordo com a o kit selecionado pelo o operador da linha de produção, após a liberação das peças será feito uma checagem de quantidade e tipo através de uma camera com auxilio de uma balança, assim visando reduzir ao máximo o erro apresentado.

### 2.2. Descrição do Problema

| O problema de     | quantidade e tipo de peças erradas                                                   |
| ----------------- | ------------------------------------------------------------------------------------ |
| afeta             | industrias que apresentam linha de montagem                                          |
| cujo o impacto é  | impossibilidade de montar o kit após o separamento das peças                         |
| uma boa solução é | criar um sistema automatizado para entregar as peças na quantidade e tipagem correta |

## 3. Descrição dos Envolvidos
### 3.1. Resumo dos Envolvidos
| Nome                                   | Descrição                                                                            | Responsabilidades                                                            |
| -------------------------------------- | ------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------- |
| Equipe de Desenvolvimento de Software  | Estudantes de Engenharia de Software da Disciplina PI 2                              | Realizar a documentação, bem como o desenvolvimento do Software do projeto.  |
| Equipe de Desenvolvimento de Hardware  | Estudantes de Engenharia de Eletrônica e de Engenharia de Energia da Disciplina PI 2 | Realizar a documentação, bem como o desenvolvimento do Hardware do projeto.  |
| Equipe de Desenvolvimento de Estrutura | Estudantes de Engenharia de Aeroespacial da Disciplina PI 2                          | Realizar a documentação, bem como o desenvolvimento da estrutura do projeto. |

### 3.2. Resumo dos Usuários
| Nome                  | Descrição                                                                     |
| --------------------- | ----------------------------------------------------------------------------- |
| Operadores da fábrica | Operadores da linha de produção, que ficam responsável pela produção dos kits |

### 3.3. Ambiente do Usuário
Será possível utilizar o site por meio de navegadores web como Mozilla Firefox, Google Chrome e Opera.

### 3.4. Perfis dos Envolvidos
#### 3.4.1. Equipe de Desenvolvimento de Software
| Representantes       | Danilo Domingo, Gabrielle Ribeiro, Heitor, Jefferson França, Matheus Henrick, Matheus Silveiro                          |
| -------------------- | ----------------------------------------------------------------------------------------------------------------------- |
| Descrição            | Realizar a documentação, bem como o desenvolvimento do Software do projeto.                                             |
| Tipo                 | Estudantes de Engenharia de Software da Disciplina PI 2                                                                 |
| Critérios de Sucesso | Entregar toda a documentação, bem como a implementação do software proposto dentro do prazo estipulado pela disciplina. |
| Envolvimento         | Alto.                                                                                                                   |

#### 3.4.2. Equipe de Desenvolvimento de Hardware
| Representantes       | Joel Jeferson, Maurício, Caio, Michael                                                                                  |
| -------------------- | ----------------------------------------------------------------------------------------------------------------------- |
| Descrição            | Realizar a documentação, bem como o desenvolvimento do Hardware do projeto.                                             |
| Tipo                 | Estudantes de Engenharia de Eletrônica e de Engenharia de Energia da Disciplina PI 2                                    |
| Critérios de Sucesso | Entregar toda a documentação, bem como a implementação do Hardware proposto dentro do prazo estipulado pela disciplina. |
| Envolvimento         | Alto.                                                                                                                   |

#### 3.4.2. Equipe de Desenvolvimento de Estrutura
| Representantes       | Ana Paula, Fernanda, Lucas                                                                                         |
| -------------------- | ------------------------------------------------------------------------------------------------------------------ |
| Descrição            | Realizar a documentação, bem como o desenvolvimento da estrutura do projeto.                                       |
| Tipo                 | Estudantes de Engenharia de Aeroespacial da Disciplina PI 2                                                        |
| Critérios de Sucesso | Entregar toda a documentação, bem como a criação da estrutura proposto dentro do prazo estipulado pela disciplina. |
| Envolvimento         | Alto.                                                                                                              |

### 3.5. Perfis dos Usuários
#### 3.5.1. Operadores da Fábrica

| Representantes       | Proprietários de uma cafeteria                                     |
| -------------------- | ------------------------------------------------------------------ |
| Tipo                 | -                                                                  |
| Responsabilidades    | Não possuem envolvimento direto com o desenvolvimento do software. |
| Critérios de Sucesso | Facilidade na montagem de kits                                     |
| Envolvimento         | Médio                                                              |

### 3.6. Principais Necessidades dos Envolvidos ou Usuários

| Representantes                  | Necessidade                                                               | Prioridade | Solução Atual                                       | Solução Proposta                                                                                   |
| ------------------------------- | ------------------------------------------------------------------------- | ---------- | --------------------------------------------------- | -------------------------------------------------------------------------------------------------- |
| Operadores da Linha de Montagem | Minimizar o erro na contagem das peças e da distribuição de peças erradas | Alta       | Processo da separação de kits feito de forma manual | Automatizar a liberação de peças para montagem de kits, com checagem no final por camera e balança |

## 4. Visão Geral do Produto
### 4.1. Perspectiva do Produto
Principais características do produto:
  * Separação das peças a partir da seleção do kit.
  * Quantidade de peças e tipos de peças definidas pela a seleção de kit.
  * Checagem do peso esperado pelo o kit com o a saída da linha através de uma balança
  * Checagem visual da quantidade e tipos de peças no final da linha, através de camera com uso de inteligência artificial para fazer a checagem.

### 4.2. Resumo das Capacidades

| Benefício para o cliente      | Recursos de Suporte                                                       |
| ----------------------------- | ------------------------------------------------------------------------- |
| Kits pré definidos            | Selecionar o kits cadastrados no sistema com a peças já definidas         |
| Separação das peças           | As peças de acordo com o kit selecionado                                  |
| Checagem das peças            | Após a separação as peças serão checadas para garantir que estão corretas |
| Interface simples e intuitiva | Essencial para não tenham dificuldades na navegação no sistema            |

### 4.3. Suposições e Dependências
Para o funcionamento do software é suposto que a equipe consiga desenvolve-lo durante o período da disciplina de PI 2. Outro ponto do projeto é o cliente, que a equipe não está desenvolvendo o projeto para um cliente específico e sim para a disciplina anteriormente citada, juntamente com isso vem o custo, que a equipe irá contribuir para acatar com o custo e fazer um protótipo para a disciplina.

### 4.4. Custo e Precificação
O produto será disponibilizado em um domínio na WEB, tendo como custo as despesas relacionadas às equipes de desenvolvimento e gerenciamento do projeto, aos valores pagos para manter seus servidores em funcionamento. Também terá uma Raspberry Pi para o funcionamento da I.A.

### 4.5. Instalação
O produto será instalado diretamente na OrangePi, assim não sendo necessário uma instalação a parte, a parte que o operador acessará será via Web, desta forma sendo necessário apenas de um dispositivo com acesso a rede da empresa.

## 5. Recursos do Produto
### 5.1. Cadastro de Operador
O cadastro de operador contém dados básicos como nome, número de identificação do operador, setor do mesmo. O operador possui acesso ao kits no sistema, peças no estoque e operação da linha de montagem.

### 5.2. Cadastro de Kits
O cadastro de kit é realizado pelo operador. Junto com o produto kit há algumas informações sobre quais peças o compõem.

### 5.3. Cadastro de Peças
O cadastro de peça é realizado pelo operador. Junto com o produto peça cadastrado há algumas informações sobre o mesmo..

### 5.4. Controle de estoque
O operador pode controlar o estoque de peças presente na esteira. Podendo fazer a recarga das peças quando elas acabarem.

### 5.5.  Acompanhamento da Separação de Peças
O operador pode acompanhar a separação, vendo quais foram usadas e no final acompanhar a checagem do kit e caso necessário fazer correções.

## 6. Restrições
  * O sistema deve está em funcionamento até o fim da disciplina.
  * Ter um dispositivo com acesso a internet.
  * Ter a Raspberry Pi com a I.A. treinada.

### 6.1 Restrições Externas
* Dentre as restrições externas, a dificuldade no uso da tecnologia e a compreensão no uso da linguagem estabelecida , além de possíveis transtornos entre a equipe.
* Durante o desenvolvimento notamos não ser possível utilizar a OrangePi para a IA, pelo fato da arquitetura ser de 32-bit, e a biblioteca necessária para o modelo ([PyTorch](https://pytorch.org/docs/stable/notes/windows.html#installation:~:text=PyTorch%20doesn%E2%80%99t%20work%20on%2032%2Dbit%20system.%20Please%20use%20Windows%20and%20Python%2064%2Dbit%20version.)) tem suporte apenas para arquiteturas de 64-bit.

### 6.2 Restrições de Design
Toda a interação com o software deve ser feita de forma natural, de modo que o operador não tenha dúvidas sobre como realizar determinada tarefa dentro do sistema. Os recursos cujo o operador têm acesso devem ser de fácil entendimento, de modo que o operador não desista durante alguma ação. Para facilitar o desenvolvimento será feito um página web.

## 7. Faixas de Qualidade
Para maior eficiência e acessibilidade, a aplicação será web, pois possibilita o acesso a aplicação que o operador acesse o sistema fora da linha, caso ele precise checar o estoque e os kits cadastrados antes de utilizar da linha.

## 8. Precedência e Prioridade
Essa etapa descreve as prioridades dos recursos do sistema e dependência de outros recursos (precedência). Para isso são definidos critérios de prioridade.

| Definição da prioridade | Descrição                                                                                                                       |
| ----------------------- | ------------------------------------------------------------------------------------------------------------------------------- |
| Crítico                 | Refere-se a um recurso essencial para o sistema, sem ele as necessidades do cliente não serão atendidas e o projeto fracassará. |
| Importante              | Não determina o fracasso do projeto, mas afetará a satisfação do usuário.                                                       |
| Útil                    | São úteis, porém não críticos. Utilizados, geralmente, com menos frequência. Não afetam a satisfação do usuário.                |

| Recurso                              | Precedência                                               | Prioridade |
| ------------------------------------ | --------------------------------------------------------- | ---------- |
| Cadastro de Operador                 | Não se aplica                                             | Crítico    |
| Cadastro de Peça                     | Cadastro de Operador                                      | Crítico    |
| Cadastro de Kits                     | Cadastro de Operador e Cadastro de Peça                   | Crítico    |
| Controle de estoque                  | Cadastro de Operador                                      | Importante |
| Acompanhamento da Separação de Peças | Cadastro de Operador, Cadastro de Peça e Cadastro de Kits | Útil       |

## Versionamento

| Data       | Versão | Descrição                     | Autor(es)                                       | Revisor        |
| ---------- | ------ | ----------------------------- | ----------------------------------------------- | -------------- |
| 25/04/2024 | 1.0    | Criação do Documento de Visão | [Danilo Domingo](https://github.com/danilow200) |
| 08/06/2024 | 2.0    | Adicionando restrição         | Heitor Marques                                  | Danilo Domingo |